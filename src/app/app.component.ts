import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  dataSource;
  constructor(private http: HttpClient) {}

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

 
  
  displayedColumns: string[];
  getNav()
  {
    this.http.get("http://13.235.106.41:3000/get-life-navigator").toPromise()
    .then((e)=>{
      let nav = JSON.parse(JSON.stringify(e));
      console.log(nav);
      this.dataSource = nav;
      this.displayedColumns = ['navigator_firstname', 'navigator_email'];
      this.dataSource.paginator = this.paginator;
    },(err)=>{

    })
    .catch((err)=>{

    });
  }
}
